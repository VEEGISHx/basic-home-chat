var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var moment = require('moment');
const notifier = require('node-notifier');
const path = require('path');

//Server
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
    console.log('[ ' + moment().format("ddd:MMM:YY") + ' ]' + '[ ' + moment().format("HH:mm:ss") + ' ]', ' - A user connected');
});

io.on('connection', function(socket) {
    socket.on('chat message', function(msg) {
        io.emit('chat message', msg);
        console.log('Message: ' + msg);
        notifier.notify({
            title: 'New message',
            message: 'Messsage: ' + msg,
            icon: path.join(__dirname, 'coulson.jpg'), // Absolute path (doesn't work on balloons) 
            sound: true, // Only Notification Center or Windows Toasters 
            wait: true // Wait with callback, until user action is taken against notification 
        }, function(err, response) {
            // Response is response from notification 
        });
    });

    socket.on('chat username', function(username) {
        io.emit('chat username', username);
    });

    socket.on('chat avatar', function(avatar) {
        io.emit('chat avatar', avatar);
    });

    socket.on('chat timestamp', function(timestamp) {
        io.emit('chat timestamp', timestamp);
    });
});

io.on('connection', function(socket) {
    socket.on('new connection', function(alive) {
        io.emit('new connection', alive)
    })
})

io.on('disconnect', function(socket) {
    socket.on('exit', function(dead) {
        io.emit('exit', dead)
    })
})

http.listen(3000, '0.0.0.0', function() {
    console.log("Server started on " + moment().format("dddd-MMMM-YYYY"));
    console.log('Listening to traffic on *:3000');
});
