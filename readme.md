# Momentale Chat Module
[![forthebadge](http://forthebadge.com/images/badges/fuck-it-ship-it.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/built-with-swag.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/certified-cousin-terio.svg)](http://forthebadge.com)

#### A small experiment with NodeJS for Momentale real-time collaborative story writing platform.

## NodeJS
* Make sure you have a stable version of node before installing.
* Download [Node v7.x](https://nodejs.org/en/download/)

## How to install:
### Using Git
* cd your/working/directory
* git clone https://VEEGISHx@bitbucket.org/VEEGISHx/momentale-chat.git
* npm install

## Server Requirements:
* [Express Framework](https://www.npmjs.com/package/express)
```bash
> npm install --save express@4.15.2
```
* [MomentJS](https://www.npmjs.com/package/moment)
```bash
> npm install moment --save
```
* [Mocha](https://www.npmjs.com/package/mocha)
```bash
> npm install moment --save
```

## Configuring the chat server:
```javascript
http.listen(<specify-port>, '0.0.0.0', function() {
    console.log("Server started on " + moment().format("dddd-MMMM-YYYY"));
    console.log('Listening to traffic on *:<specify-port>');
});
```

## Testing
#### Test dependency - Mocha
#### Test command - npm test
##### npm test should return something like this:
```bash
> mocha


Veegish@xVee:/mnt/c/<workingdirectory>/momentale-chat$ npm test

> nodejs-socket-test@0.0.1 test /mnt/c/<workingdirectory>/momentale-chat
> mocha



  Array
    #indexOf()
      ✓ should return -1 when the value is not present


  1 passing (7ms)
```
## Launching the server
```javascript
> node app.js
```

#### Server is accessible by default on http://localhost:3000
